#!/bin/sh

set -e

# Configure docroot.
sed -i 's@%VARNISH_HOST%@'"${VARNISH_HOST}"'@' /etc/varnish/default.vcl
echo "Configured Varnish host to ${VARNISH_HOST}."

sed -i 's@%VARNISH_PORT%@'"${VARNISH_PORT}"'@' /etc/varnish/default.vcl
echo "Configured Varnish host to ${VARNISH_PORT}."

sed -i 's@%LOCAL_IP%@'"${LOCAL_IP}"'@' /etc/varnish/default.vcl
echo "Added ${LOCAL_IP} to ACL PURGE list."

exec sh -c \
  "exec varnishd -F \
  -f $VCL_CONFIG \
  -s malloc,$CACHE_SIZE \
  -a :80 -a :6086,PROXY \
  $VARNISHD_PARAMS"
